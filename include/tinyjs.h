/*
 * TinyJS
 *
 * A single-file Javascript-alike engine
 *
 * Authored By Gordon Williams <gw@pur3.co.uk>
 *
 * Copyright (C) 2009 Pur3 Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

// If defined, this keeps a note of all calls and where from in memory. This is slower, but good for debugging
#define TINYJS_CALL_STACK

#ifdef _WIN32
    #ifdef _DEBUG
        #define _CRTDBG_MAP_ALLOC
        #include <stdlib.h>
        #include <crtdbg.h>
    #endif
#endif
#include <string>
#include <vector>

#ifndef TRACE
    #define TRACE printf
#endif // TRACE


namespace TinyJS
{

    const int TINYJS_LOOP_MAX_ITERATIONS = 8192;

    enum LEX_TYPES
    {
        LEX_EOF = 0,
        LEX_ID = 256,
        LEX_INT,
        LEX_FLOAT,
        LEX_STR,

        LEX_EQUAL,
        LEX_TYPEEQUAL,
        LEX_NEQUAL,
        LEX_NTYPEEQUAL,
        LEX_LEQUAL,
        LEX_LSHIFT,
        LEX_LSHIFTEQUAL,
        LEX_GEQUAL,
        LEX_RSHIFT,
        LEX_RSHIFTUNSIGNED,
        LEX_RSHIFTEQUAL,
        LEX_PLUSEQUAL,
        LEX_MINUSEQUAL,
        LEX_PLUSPLUS,
        LEX_MINUSMINUS,
        LEX_ANDEQUAL,
        LEX_ANDAND,
        LEX_OREQUAL,
        LEX_OROR,
        LEX_XOREQUAL,
        // reserved words
    #define LEX_R_LIST_START LEX_R_IF
        LEX_R_IF,
        LEX_R_ELSE,
        LEX_R_DO,
        LEX_R_WHILE,
        LEX_R_FOR,
        LEX_R_BREAK,
        LEX_R_CONTINUE,
        LEX_R_FUNCTION,
        LEX_R_RETURN,
        LEX_R_VAR,
        LEX_R_TRUE,
        LEX_R_FALSE,
        LEX_R_NULL,
        LEX_R_UNDEFINED,
        LEX_R_NEW,
        LEX_R_LIST_END /* always the last entry */
    };

    enum SCRIPTVAR_FLAGS
    {
        SCRIPTVAR_UNDEFINED   = 0,
        SCRIPTVAR_FUNCTION    = 1,
        SCRIPTVAR_OBJECT      = 2,
        SCRIPTVAR_ARRAY       = 4,
        SCRIPTVAR_DOUBLE      = 8,  // floating point double
        SCRIPTVAR_INTEGER     = 16, // integer number
        SCRIPTVAR_STRING      = 32, // string
        SCRIPTVAR_NULL        = 64, // it seems null is its own data type
        SCRIPTVAR_NATIVE      = 128, // to specify this is a native function
        SCRIPTVAR_NUMERICMASK =
            SCRIPTVAR_NULL |
            SCRIPTVAR_DOUBLE |
            SCRIPTVAR_INTEGER,
        SCRIPTVAR_VARTYPEMASK =
            SCRIPTVAR_DOUBLE |
            SCRIPTVAR_INTEGER |
            SCRIPTVAR_STRING |
            SCRIPTVAR_FUNCTION |
            SCRIPTVAR_OBJECT |
            SCRIPTVAR_ARRAY |
            SCRIPTVAR_NULL,

    };

    #define TINYJS_RETURN_VAR "return"
    #define TINYJS_PROTOTYPE_CLASS "prototype"
    #define TINYJS_TEMP_NAME ""
    #define TINYJS_BLANK_DATA ""

    class RuntimeError
    {
        public:
            std::string text;
            RuntimeError(const std::string &exceptionText);
    };

    class Lexer
    {
        protected:
            /*
            * When we go into a loop, we use getSubLex to get a lexer for just the sub-part of the
            * relevant string. This doesn't re-allocate and copy the string, but instead copies
            * the data pointer and sets dataOwned to false, and dataStart/dataEnd to the relevant things.
            */
            /// Data string to get tokens from
            char* m_data;

            /// Start and end position in data string
            int m_dataStart;
            int m_dataEnd;

            /// Do we own this data string?
            bool m_dataOwned;

            /// Position in data (we CAN go past the end of the string here)
            int m_dataPos;


        public:
            char m_currCh;
            char m_nextCh;
            /// The type of the token that we have
            int m_tk;
            /// Position in the data at the beginning of the token we have here
            int m_tokenStart;
             /// Position in the data at the last character of the token we have here
            int m_tokenEnd;
            /// Position in the data at the last character of the last token
            int m_tokenLastEnd;
            /// Data contained in the token we have here
            std::string m_tkStr;

        protected:
            void getNextCh();
            /// Get the text token from our text string
            void getNextToken();

        public:
            Lexer(const std::string &input);
            Lexer(Lexer *owner, int startChar, int endChar);
            ~Lexer(void);


            /// Lexical match wotsit
            void match(int expected_tk);

            /// Get the string representation of the given token
            static std::string getTokenStr(int token);

            /// Reset this lex so we can start again
            void reset(); 

            /// Return a sub-string from the given position up until right now
            std::string getSubString(int pos);

            /// Return a sub-lexer from the given position up until right now
            Lexer *getSubLex(int lastPosition);

            /// Return a string representing the position in lines and columns of the character pos given
            std::string getPosition(int pos=-1); 
    };

    class Variable;

    typedef void (*JSCallback)(Variable *var, void *userdata);

    class VarLink
    {
        public:
          std::string name;
          VarLink *nextSibling;
          VarLink *prevSibling;
          Variable *var;
          bool owned;

        public:
            VarLink(Variable *var, const std::string &name = TINYJS_TEMP_NAME);
            /// Copy constructor
            VarLink(const VarLink &link);
            ~VarLink();
            /// Replace the Variable pointed to
            void replaceWith(Variable *newVar);
            /// Replace the Variable pointed to (just dereferences)
            void replaceWith(VarLink *newVar);
            /// Get the name as an integer (for arrays)
            int getIntName();
            /// Set the name as an integer (for arrays)
            void setIntName(int n);
    };

    /// Variable class (containing a doubly-linked list of children)
    class Variable
    {
        public:
            VarLink *firstChild;
            VarLink *lastChild;

        public:
            /// Create undefined
            Variable();

            /// User defined
            Variable(const std::string &varData, int varFlags);

            /// Create a string
            Variable(const std::string &str);
            Variable(double varData);
            Variable(int val);
            ~Variable(void);

            /// If this is a function, get the result value (for use by native functions)
            Variable *getReturnVar();

            /// Set the result value. Use this when setting complex return data as it avoids a deepCopy()
            void setReturnVar(Variable *var);

            /// If this is a function, get the parameter with the given name (for use by native functions)
            Variable *getParameter(const std::string &name);

            /// Tries to find a child with the given name, may return 0
            VarLink *findChild(const std::string &childName);

            /// Tries to find a child with the given name, or will create it with the given flags
            VarLink *findChildOrCreate(const std::string &childName, int varFlags=SCRIPTVAR_UNDEFINED);

            ///< Tries to find a child with the given path (separated by dots)
            VarLink *findChildOrCreateByPath(const std::string &path);

            ///< add a child overwriting any with the same name
            VarLink *addChild(const std::string &childName, Variable *child=NULL);
            VarLink *addChildNoDup(const std::string &childName, Variable *child=NULL);
            void removeChild(Variable *child);

            /// Remove a specific link (this is faster than finding via a child)
            void removeLink(VarLink *link);
            void removeAllChildren();

            ///< The the value at an array index
            Variable *getArrayIndex(int idx);

            ///< Set the value at an array index
            void setArrayIndex(int idx, Variable *value);

            ///< If this is an array, return the number of items in it (else 0)
            int getArrayLength();

            /// Get the number of children
            int getChildren();

            /// get data from variable
            int getInt();
            bool getBool()
            {
                return getInt() != 0;
            }
            double getDouble();
            const std::string& getString();

            ///< get Data as a parsable javascript string
            std::string getParsableString();

            /// set data for variable
            void setInt(int num);
            void setDouble(double val);
            void setString(const std::string &str);
            void setUndefined();
            void setArray();

            
            bool equals(Variable *v);

            bool isInt();
            bool isDouble();
            bool isString();
            bool isNumeric();
            bool isFunction();
            bool isObject();
            bool isArray();
            bool isNative();
            bool isUndefined();
            bool isNull();

            /// Is this *not* an array/object/etc
            bool isBasic();

            /// do a maths op with another script variable
            Variable *mathsOp(Variable *b, int op);

            ///< copy the value from the value given
            void copyValue(Variable *val);

            /// deep copy this node and return the result
            Variable *deepCopy();

            /// Dump out the contents of this using trace
            void trace(std::string indentStr = "", const std::string &name = "");

            /// For debugging - just dump a string version of the flags
            std::string getFlagsAsString();

            ///< Write out all the JS code needed to recreate this script variable to the stream (as JSON)
            void getJSON(std::ostringstream &destination, const std::string linePrefix="");

            /// Set the callback for native functions
            void setCallback(JSCallback callback, void *userdata);


            /// For memory management/garbage collection
            Variable *ref(); ///< Add reference to this variable
            void unref(); ///< Remove a reference, and delete this variable if required
            int getRefs(); ///< Get the number of references to this script variable
        protected:
            int refs; ///< The number of references held to this - used for garbage collection

            std::string data; ///< The contents of this variable if it is a string
            long intData; ///< The contents of this variable if it is an int
            double doubleData; ///< The contents of this variable if it is a double
            int flags; ///< the flags determine the type of the variable - int/double/string/etc
            JSCallback jsCallback; ///< Callback for native functions
            void *jsCallbackUserData; ///< user data passed as second argument to native functions

            void init(); ///< initialisation of data members

            /** Copy the basic data and flags from the variable given, with no
              * children. Should be used internally only - by copyValue and deepCopy */
            void copySimpleData(Variable *val);

            friend class Interpreter;
    };

    class Interpreter
    {
        private:
            /// current lexer
            Lexer* m_lexer;

            /// stack of scopes when parsing
            std::vector<Variable*> m_scopes;

        #ifdef TINYJS_CALL_STACK
            /// Names of places called so we can show when erroring
            std::vector<std::string> m_callstack;
        #endif

            /// Built in string class
            Variable* m_stringclass;

            /// Built in object class
            Variable* m_objectclass;

            /// Built in array class
            Variable* m_arrayclass;

            /// root of symbol table
            Variable* m_roottable;


        private:
            // parsing - in order of precedence
            VarLink *functionCall(
                bool &execute, VarLink *function, Variable *parent);
            VarLink *factor(bool &execute);
            VarLink *unary(bool &execute);
            VarLink *term(bool &execute);
            VarLink *expression(bool &execute);
            VarLink *shift(bool &execute);
            VarLink *condition(bool &execute);
            VarLink *logic(bool &execute);
            VarLink *ternary(bool &execute);
            VarLink *base(bool &execute);
            void block(bool &execute);
            void statement(bool &execute);
            // parsing utility functions
            VarLink *parseFunctionDefinition();
            void parseFunctionArguments(Variable *funcVar);

            VarLink *findInScopes(const std::string &childName); ///< Finds a child, looking recursively up the scopes
            /// Look up in any parent classes of the given object
            VarLink *findInParentClasses(Variable *object, const std::string &name);

        public:
            Interpreter();
            ~Interpreter();

            void execute(const std::string &code);
            /**
            * Evaluate the given code and return a link to a javascript object,
            * useful for (dangerous) JSON parsing. If nothing to return, will return
            * 'undefined' variable type. VarLink is returned as this will
            * automatically unref the result as it goes out of scope. If you want to
            * keep it, you must use ref() and unref() */
            VarLink evaluateComplex(const std::string &code);

            /**
            *  Evaluate the given code and return a string.
            *  If nothing to return, will return
            * 'undefined'
            */
            std::string evaluate(const std::string &code);

            /// add a native function to be called from TinyJS
            /** example:
               \code
                   void scRandInt(Variable *c, void *userdata) { ... }
                   tinyJS->addNative("function randInt(min, max)", scRandInt, 0);
               \endcode

               or

               \code
                   void scSubstring(Variable *c, void *userdata) { ... }
                   tinyJS->addNative("function String.substring(lo, hi)", scSubstring, 0);
               \endcode
            */
            void addNative(const std::string &funcDesc, JSCallback ptr, void *userdata);

            /// Get the given variable specified by a path (var1.var2.etc), or return 0
            Variable *getScriptVariable(const std::string &path);
            /// Get the value of the given variable, or return 0
            const std::string *getVariable(const std::string &path);
            /// set the value of the given variable, return trur if it exists and gets set
            bool setVariable(const std::string &path, const std::string &varData);

            /// Send all variables to stdout
            void trace();

            /// get roottable
            Variable* getRoot();
    };

    /// Register useful functions with the TinyJS interpreter
    extern void registerFunctions(Interpreter *tinyJS);
    void registerMathFunctions(Interpreter *tinyJS);
}
