
libname         = tinyjs
exename_main    = tjs
exename_testbin = run_tests

# sources for tinyjs
sources =  \
	src/util.cpp \
	src/scriptvar.cpp \
	src/varlink.cpp \
	src/exception.cpp \
	src/lexer.cpp \
	src/tinyjs.cpp \
	src/functions.cpp \
	src/mathfuncs.cpp

# sources for the tinyjs frontend
sources_main = \
	src/main.cpp

# sources for the unittest program
sources_testbin = \
	src/run_tests.cpp

# names for intermediate files
objects         = $(sources:.cpp=.o)
objects_main    = $(sources_main:.cpp=.o)
objects_testbin = $(sources_testbin:.cpp=.o)

#############################################
### warning: edit below at your own risk! ###
#############################################

# construct the physical library file from the library name
libfile = lib$(libname).a

# need C++11 support (not going to change, sorry folks)
CXX = clang++ -std=c++11

# flags for header file paths
INCFLAGS = -Iinclude

# remove underscore for heavy-duty checks.
# WARNING: VERBOSE
_WFLAGS = \
	-pedantic -Wall -Wextra \
	-Wcast-align \
	-Wcast-qual \
	-Wctor-dtor-privacy \
	-Wdisabled-optimization \
	-Wformat=2 \
	-Winit-self \
	-Wmissing-declarations \
	-Wold-style-cast \
	-Woverloaded-virtual \
	-Wredundant-decls \
	-Wshadow \
	-Wsign-conversion \
	-Wsign-promo \
	-Wstrict-overflow=5 \
	-Wswitch-default \
	-Wundef \
	-Wno-unused

# compiler flags
CFLAGS = $(INCFLAGS) $(WFLAGS) -c -g -rdynamic -D_DEBUG

# linker flags
LDFLAGS = -g -rdynamic -L. -Wl,-rpath=.
BINFLAGS = -l$(libname) $(LDFLAGS)

##########################
#### tasks begin here ####
##########################

# 'default' task
all: $(objects) $(libfile) $(exename_testbin) $(exename_main)

# task for building the libfile (static or dynamic -- modify accordingly!)
$(libfile): $(objects)
	ar rcs $(libfile) $(objects)

# task for building our frontend executable
$(exename_main): $(libfile) $(objects_main)
	$(CXX) -o $(exename_main) $(objects_main) $(BINFLAGS)

# task for building our unittesting application
$(exename_testbin): $(libfile) $(objects_testbin)
	$(CXX) -o $(exename_testbin) $(objects_testbin) $(BINFLAGS)

# task for cleaning up intermediate files
clean:
	rm -f $(objects)
	rm -f $(objects_main)
	rm -f $(objects_testbin)

# task for cleaning up everything else as well, leaving
# us with the sources only (hence **dist**clean)
distclean: clean
	rm -f $(libfile)
	rm -f $(exename_main)
	rm -f $(exename_testbin)

# invoke distclean, and rebuild every binary
rebuild: distclean all

# the 'sed' part here is to strip color codes from the diagnostic
# output -- specifically from clang. only reason being that NppExec
# (yes, the Notepad++ plugin) can't handle it. sorry :-)
.cpp.o:
	$(CXX) $(CFLAGS) $< -o $@ 2>&1 | sed 's/\o33\[30m/\o33[37m/g'

