
#include "private.h"

namespace TinyJS
{
    Variable::Variable()
    {
        refs = 0;
    #if DEBUG_MEMORY
        mark_allocated(this);
    #endif
        init();
        flags = SCRIPTVAR_UNDEFINED;
    }

    Variable::Variable(const std::string& str)
    {
        refs = 0;
    #if DEBUG_MEMORY
        mark_allocated(this);
    #endif
        init();
        flags = SCRIPTVAR_STRING;
        data = str;
    }

    Variable::Variable(const std::string& varData, int varFlags)
    {
        refs = 0;
    #if DEBUG_MEMORY
        mark_allocated(this);
    #endif
        init();
        flags = varFlags;
        if(varFlags & SCRIPTVAR_INTEGER)
        {
            intData = strtol(varData.c_str(),0,0);
        }
        else if(varFlags & SCRIPTVAR_DOUBLE)
        {
            doubleData = strtod(varData.c_str(),0);
        }
        else
        {
            data = varData;
        }
    }

    Variable::Variable(double val)
    {
        refs = 0;
    #if DEBUG_MEMORY
        mark_allocated(this);
    #endif
        init();
        setDouble(val);
    }

    Variable::Variable(int val)
    {
        refs = 0;
    #if DEBUG_MEMORY
        mark_allocated(this);
    #endif
        init();
        setInt(val);
    }

    Variable::~Variable(void)
    {
    #if DEBUG_MEMORY
        mark_deallocated(this);
    #endif
        removeAllChildren();
    }

    void Variable::init()
    {
        firstChild = 0;
        lastChild = 0;
        flags = 0;
        jsCallback = 0;
        jsCallbackUserData = 0;
        data = TINYJS_BLANK_DATA;
        intData = 0;
        doubleData = 0;
    }

    Variable* Variable::getReturnVar()
    {
        return getParameter(TINYJS_RETURN_VAR);
    }

    void Variable::setReturnVar(Variable* var)
    {
        findChildOrCreate(TINYJS_RETURN_VAR)->replaceWith(var);
    }

    Variable* Variable::getParameter(const std::string& name)
    {
        return findChildOrCreate(name)->var;
    }

    VarLink* Variable::findChild(const std::string& childName)
    {
        VarLink* v = firstChild;
        while(v)
        {
            if(v->name.compare(childName)==0)
            {
                return v;
            }
            v = v->nextSibling;
        }
        return 0;
    }

    VarLink* Variable::findChildOrCreate(const std::string& childName, int varFlags)
    {
        VarLink* l = findChild(childName);
        if(l)
        {
            return l;
        }
        return addChild(childName, new Variable(TINYJS_BLANK_DATA, varFlags));
    }

    VarLink* Variable::findChildOrCreateByPath(const std::string& path)
    {
        size_t p = path.find('.');
        if(p == std::string::npos)
        {
            return findChildOrCreate(path);
        }
        return findChildOrCreate(path.substr(0,p), SCRIPTVAR_OBJECT)->var->
               findChildOrCreateByPath(path.substr(p+1));
    }

    VarLink* Variable::addChild(const std::string& childName, Variable* child)
    {
        if(isUndefined())
        {
            flags = SCRIPTVAR_OBJECT;
        }
        // if no child supplied, create one
        if(!child)
        {
            child = new Variable();
        }
        VarLink* link = new VarLink(child, childName);
        link->owned = true;
        if(lastChild)
        {
            lastChild->nextSibling = link;
            link->prevSibling = lastChild;
            lastChild = link;
        }
        else
        {
            firstChild = link;
            lastChild = link;
        }
        return link;
    }

    VarLink* Variable::addChildNoDup(const std::string& childName, Variable* child)
    {
        // if no child supplied, create one
        if(!child)
        {
            child = new Variable();
        }
        VarLink* v = findChild(childName);
        if(v)
        {
            v->replaceWith(child);
        }
        else
        {
            v = addChild(childName, child);
        }
        return v;
    }

    void Variable::removeChild(Variable* child)
    {
        VarLink* link = firstChild;
        while(link)
        {
            if(link->var == child)
            {
                break;
            }
            link = link->nextSibling;
        }
        ASSERT(link);
        removeLink(link);
    }

    void Variable::removeLink(VarLink* link)
    {
        if(!link)
        {
            return;
        }
        if(link->nextSibling)
        {
            link->nextSibling->prevSibling = link->prevSibling;
        }
        if(link->prevSibling)
        {
            link->prevSibling->nextSibling = link->nextSibling;
        }
        if(lastChild == link)
        {
            lastChild = link->prevSibling;
        }
        if(firstChild == link)
        {
            firstChild = link->nextSibling;
        }
        delete link;
    }

    void Variable::removeAllChildren()
    {
        VarLink* c = firstChild;
        while(c)
        {
            VarLink* t = c->nextSibling;
            delete c;
            c = t;
        }
        firstChild = 0;
        lastChild = 0;
    }

    Variable* Variable::getArrayIndex(int idx)
    {
        char sIdx[64];
        sprintf_s(sIdx, sizeof(sIdx), "%d", idx);
        VarLink* link = findChild(sIdx);
        if(link)
        {
            return link->var;
        }
        else
        {
            return new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_NULL);    // undefined
        }
    }

    void Variable::setArrayIndex(int idx, Variable* value)
    {
        char sIdx[64];
        sprintf_s(sIdx, sizeof(sIdx), "%d", idx);
        VarLink* link = findChild(sIdx);
        if(link)
        {
            if(value->isUndefined())
            {
                removeLink(link);
            }
            else
            {
                link->replaceWith(value);
            }
        }
        else
        {
            if(!value->isUndefined())
            {
                addChild(sIdx, value);
            }
        }
    }

    int Variable::getArrayLength()
    {
        int highest = -1;
        if(!isArray())
        {
            return 0;
        }
        VarLink* link = firstChild;
        while(link)
        {
            if(isNumber(link->name))
            {
                int val = atoi(link->name.c_str());
                if(val > highest)
                {
                    highest = val;
                }
            }
            link = link->nextSibling;
        }
        return highest+1;
    }

    int Variable::getChildren()
    {
        int n = 0;
        VarLink* link = firstChild;
        while(link)
        {
            n++;
            link = link->nextSibling;
        }
        return n;
    }

    int Variable::getInt()
    {
        /* strtol understands about hex and octal */
        if(isInt())
        {
            return intData;
        }
        if(isNull())
        {
            return 0;
        }
        if(isUndefined())
        {
            return 0;
        }
        if(isDouble())
        {
            return (int)doubleData;
        }
        return 0;
    }

    double Variable::getDouble()
    {
        if(isDouble())
        {
            return doubleData;
        }
        if(isInt())
        {
            return intData;
        }
        if(isNull())
        {
            return 0;
        }
        if(isUndefined())
        {
            return 0;
        }
        return 0; /* or NaN? */
    }

    const std::string& Variable::getString()
    {
        /* Because we can't return a std::string that is generated on demand.

         * I should really just use char* :) */
        static std::string s_null = "null";
        static std::string s_undefined = "undefined";
        if(isInt())
        {
            char buffer[32];
            sprintf_s(buffer, sizeof(buffer), "%ld", intData);
            data = buffer;
            return data;
        }
        if(isDouble())
        {
            char buffer[32];
            sprintf_s(buffer, sizeof(buffer), "%f", doubleData);
            data = buffer;
            return data;
        }
        if(isNull())
        {
            return s_null;
        }
        if(isUndefined())
        {
            return s_undefined;
        }
        // are we just a std::string here?
        return data;
    }

    void Variable::setInt(int val)
    {
        flags = (flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_INTEGER;
        intData = val;
        doubleData = 0;
        data = TINYJS_BLANK_DATA;
    }

    void Variable::setDouble(double val)
    {
        flags = (flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_DOUBLE;
        doubleData = val;
        intData = 0;
        data = TINYJS_BLANK_DATA;
    }

    void Variable::setString(const std::string& str)
    {
        // name sure it's not still a number or integer
        flags = (flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_STRING;
        data = str;
        intData = 0;
        doubleData = 0;
    }

    void Variable::setUndefined()
    {
        // name sure it's not still a number or integer
        flags = (flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_UNDEFINED;
        data = TINYJS_BLANK_DATA;
        intData = 0;
        doubleData = 0;
        removeAllChildren();
    }

    void Variable::setArray()
    {
        // name sure it's not still a number or integer
        flags = (flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_ARRAY;
        data = TINYJS_BLANK_DATA;
        intData = 0;
        doubleData = 0;
        removeAllChildren();
    }

    bool Variable::equals(Variable* v)
    {
        Variable* resV = mathsOp(v, LEX_EQUAL);
        bool res = resV->getBool();
        delete resV;
        return res;
    }

    Variable* Variable::mathsOp(Variable* b, int op)
    {
        Variable* a = this;
        // Type equality check
        if(op == LEX_TYPEEQUAL || op == LEX_NTYPEEQUAL)
        {
            // check type first, then call again to check data
            bool eql = ((a->flags & SCRIPTVAR_VARTYPEMASK) ==
                        (b->flags & SCRIPTVAR_VARTYPEMASK));
            if(eql)
            {
                Variable* contents = a->mathsOp(b, LEX_EQUAL);
                if(!contents->getBool())
                {
                    eql = false;
                }
                if(!contents->refs)
                {
                    delete contents;
                }
            }
            ;
            if(op == LEX_TYPEEQUAL)
            {
                return new Variable(eql);
            }
            else
            {
                return new Variable(!eql);
            }
        }
        // do maths...
        if(a->isUndefined() && b->isUndefined())
        {
            if(op == LEX_EQUAL)
            {
                return new Variable(true);
            }
            else if(op == LEX_NEQUAL)
            {
                return new Variable(false);
            }
            else
            {
                return new Variable();    // undefined
            }
        }
        else if((a->isNumeric() || a->isUndefined()) &&
                (b->isNumeric() || b->isUndefined()))
        {
            if(!a->isDouble() && !b->isDouble())
            {
                // use ints
                int da = a->getInt();
                int db = b->getInt();
                switch(op)
                {
                    case '+':
                        return new Variable(da+db);
                    case '-':
                        return new Variable(da-db);
                    case '*':
                        return new Variable(da*db);
                    case '/':
                        return new Variable(da/db);
                    case '&':
                        return new Variable(da&db);
                    case '|':
                        return new Variable(da|db);
                    case '^':
                        return new Variable(da^db);
                    case '%':
                        return new Variable(da%db);
                    case LEX_EQUAL:
                        return new Variable(da==db);
                    case LEX_NEQUAL:
                        return new Variable(da!=db);
                    case '<':
                        return new Variable(da<db);
                    case LEX_LEQUAL:
                        return new Variable(da<=db);
                    case '>':
                        return new Variable(da>db);
                    case LEX_GEQUAL:
                        return new Variable(da>=db);
                    default:
                        throw new RuntimeError("Operation "+Lexer::getTokenStr(op)+" not supported on the Int datatype");
                }
            }
            else
            {
                // use doubles
                double da = a->getDouble();
                double db = b->getDouble();
                switch(op)
                {
                    case '+':
                        return new Variable(da+db);
                    case '-':
                        return new Variable(da-db);
                    case '*':
                        return new Variable(da*db);
                    case '/':
                        return new Variable(da/db);
                    case LEX_EQUAL:
                        return new Variable(da==db);
                    case LEX_NEQUAL:
                        return new Variable(da!=db);
                    case '<':
                        return new Variable(da<db);
                    case LEX_LEQUAL:
                        return new Variable(da<=db);
                    case '>':
                        return new Variable(da>db);
                    case LEX_GEQUAL:
                        return new Variable(da>=db);
                    default:
                        throw new RuntimeError("Operation "+Lexer::getTokenStr(op)+" not supported on the Double datatype");
                }
            }
        }
        else if(a->isArray())
        {
            /* Just check pointers */
            switch(op)
            {
                case LEX_EQUAL:
                    return new Variable(a==b);
                case LEX_NEQUAL:
                    return new Variable(a!=b);
                default:
                    throw new RuntimeError("Operation "+Lexer::getTokenStr(op)+" not supported on the Array datatype");
            }
        }
        else if(a->isObject())
        {
            /* Just check pointers */
            switch(op)
            {
                case LEX_EQUAL:
                    return new Variable(a==b);
                case LEX_NEQUAL:
                    return new Variable(a!=b);
                default:
                    throw new RuntimeError("Operation "+Lexer::getTokenStr(op)+" not supported on the Object datatype");
            }
        }
        else
        {
            std::string da = a->getString();
            std::string db = b->getString();
            // use std::strings
            switch(op)
            {
                case '+':
                    return new Variable(da+db, SCRIPTVAR_STRING);
                case LEX_EQUAL:
                    return new Variable(da==db);
                case LEX_NEQUAL:
                    return new Variable(da!=db);
                case '<':
                    return new Variable(da<db);
                case LEX_LEQUAL:
                    return new Variable(da<=db);
                case '>':
                    return new Variable(da>db);
                case LEX_GEQUAL:
                    return new Variable(da>=db);
                default:
                    throw new RuntimeError("Operation "+Lexer::getTokenStr(op)+" not supported on the std::string datatype");
            }
        }
        ASSERT(0);
        return 0;
    }

    void Variable::copySimpleData(Variable* val)
    {
        data = val->data;
        intData = val->intData;
        doubleData = val->doubleData;
        flags = (flags & ~SCRIPTVAR_VARTYPEMASK) | (val->flags & SCRIPTVAR_VARTYPEMASK);
    }

    void Variable::copyValue(Variable* val)
    {
        if(val)
        {
            copySimpleData(val);
            // remove all current children
            removeAllChildren();
            // copy children of 'val'
            VarLink* child = val->firstChild;
            while(child)
            {
                Variable* copied;
                // don't copy the 'parent' object...
                if(child->name != TINYJS_PROTOTYPE_CLASS)
                {
                    copied = child->var->deepCopy();
                }
                else
                {
                    copied = child->var;
                }
                addChild(child->name, copied);
                child = child->nextSibling;
            }
        }
        else
        {
            setUndefined();
        }
    }

    Variable* Variable::deepCopy()
    {
        Variable* newVar = new Variable();
        newVar->copySimpleData(this);
        // copy children
        VarLink* child = firstChild;
        while(child)
        {
            Variable* copied;
            // don't copy the 'parent' object...
            if(child->name != TINYJS_PROTOTYPE_CLASS)
            {
                copied = child->var->deepCopy();
            }
            else
            {
                copied = child->var;
            }
            newVar->addChild(child->name, copied);
            child = child->nextSibling;
        }
        return newVar;
    }

    void Variable::trace(std::string indentStr, const std::string& name)
    {
        TRACE("%s'%s' = '%s' %s\n",
              indentStr.c_str(),
              name.c_str(),
              getString().c_str(),
              getFlagsAsString().c_str());
        std::string indent = indentStr+" ";
        VarLink* link = firstChild;
        while(link)
        {
            link->var->trace(indent, link->name);
            link = link->nextSibling;
        }
    }

    std::string Variable::getFlagsAsString()
    {
        std::string flagstr = "";
        if(flags&SCRIPTVAR_FUNCTION)
        {
            flagstr = flagstr + "FUNCTION ";
        }
        if(flags&SCRIPTVAR_OBJECT)
        {
            flagstr = flagstr + "OBJECT ";
        }
        if(flags&SCRIPTVAR_ARRAY)
        {
            flagstr = flagstr + "ARRAY ";
        }
        if(flags&SCRIPTVAR_NATIVE)
        {
            flagstr = flagstr + "NATIVE ";
        }
        if(flags&SCRIPTVAR_DOUBLE)
        {
            flagstr = flagstr + "DOUBLE ";
        }
        if(flags&SCRIPTVAR_INTEGER)
        {
            flagstr = flagstr + "INTEGER ";
        }
        if(flags&SCRIPTVAR_STRING)
        {
            flagstr = flagstr + "STRING ";
        }
        return flagstr;
    }

    std::string Variable::getParsableString()
    {
        // Numbers can just be put in directly
        if(isNumeric())
        {
            return getString();
        }
        if(isFunction())
        {
            std::stringstream funcStr;
            funcStr << "function (";
            // get list of parameters
            VarLink* link = firstChild;
            while(link)
            {
                funcStr << link->name;
                if(link->nextSibling)
                {
                    funcStr << ",";
                }
                link = link->nextSibling;
            }
            // add function body
            funcStr << ") " << getString();
            return funcStr.str();
        }
        // if it is a std::string then we quote it
        if(isString())
        {
            return getJSString(getString());
        }
        if(isNull())
        {
            return "null";
        }
        return "undefined";
    }

    void Variable::getJSON(std::ostringstream& destination, const std::string linePrefix)

    {
        if(isObject())
        {
            std::string indentedLinePrefix = linePrefix+"  ";
            // children - handle with bracketed list
            destination << "{ \n";
            VarLink* link = firstChild;
            while(link)
            {
                destination << indentedLinePrefix;
                destination  << getJSString(link->name);
                destination  << " : ";
                link->var->getJSON(destination, indentedLinePrefix);
                link = link->nextSibling;
                if(link)
                {
                    destination  << ",\n";
                }
            }
            destination << "\n" << linePrefix << "}";
        }
        else if(isArray())
        {
            std::string indentedLinePrefix = linePrefix+"  ";
            destination << "[\n";
            int len = getArrayLength();
            if(len>10000)
            {
                len=10000;    // we don't want to get stuck here!
            }
            for(int i=0; i<len; i++)
            {
                getArrayIndex(i)->getJSON(destination, indentedLinePrefix);
                if(i<len-1)
                {
                    destination  << ",\n";
                }
            }
            destination << "\n" << linePrefix << "]";
        }
        else
        {
            // no children or a function... just write value directly
            destination << getParsableString();
        }
    }

    void Variable::setCallback(JSCallback callback, void* userdata)
    {
        jsCallback = callback;
        jsCallbackUserData = userdata;
    }

    Variable* Variable::ref()
    {
        refs++;
        return this;
    }

    void Variable::unref()
    {
        if(refs<=0)
        {
            printf("OMFG, we have unreffed too far!\n");
        }
        if((--refs)==0)
        {
            delete this;
        }
    }

    int Variable::getRefs()
    {
        return refs;
    }

    bool Variable::isInt()
    {
        return (flags & SCRIPTVAR_INTEGER) != 0;
    }

    bool Variable::isDouble()
    {
        return (flags&SCRIPTVAR_DOUBLE)!=0;
    }

    bool Variable::isString()
    {
        return (flags&SCRIPTVAR_STRING)!=0;
    }

    bool Variable::isNumeric()
    {
        return (flags&SCRIPTVAR_NUMERICMASK)!=0;
    }

    bool Variable::isFunction()
    {
        return (flags&SCRIPTVAR_FUNCTION)!=0;
    }

    bool Variable::isObject()
    {
        return (flags&SCRIPTVAR_OBJECT)!=0;
    }

    bool Variable::isArray()
    {
        return (flags&SCRIPTVAR_ARRAY)!=0;
    }

    bool Variable::isNative()
    {
        return (flags&SCRIPTVAR_NATIVE)!=0;
    }

    bool Variable::isUndefined()
    {
        return (flags & SCRIPTVAR_VARTYPEMASK) == SCRIPTVAR_UNDEFINED;
    }

    bool Variable::isNull()
    {
        return (flags & SCRIPTVAR_NULL)!=0;
    }

    bool Variable::isBasic()
    {
        return firstChild==0;
    }
}

