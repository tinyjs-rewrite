
#include "private.h"

namespace TinyJS
{
    Lexer::Lexer(const std::string& input)
    {
        m_data = strncopy(input.c_str(), input.size());
        m_dataOwned = true;
        m_dataStart = 0;
        m_dataEnd = int(input.size());
        reset();
    }

    Lexer::Lexer(Lexer* owner, int startChar, int endChar)
    {
        m_data = owner->m_data;
        m_dataOwned = false;
        m_dataStart = startChar;
        m_dataEnd = endChar;
        reset();
    }

    Lexer::~Lexer(void)
    {
        if(m_dataOwned)
        {
            free((void*)m_data);
        }
    }

    void Lexer::reset()
    {
        m_dataPos = m_dataStart;
        m_tokenStart = 0;
        m_tokenEnd = 0;
        m_tokenLastEnd = 0;
        m_tk = 0;
        m_tkStr = "";
        getNextCh();
        getNextCh();
        getNextToken();
    }

    void Lexer::match(int expected_tk)
    {
        if(m_tk != expected_tk)
        {
            std::stringstream errorString;
            errorString << "Got " << getTokenStr(m_tk) << " expected " << getTokenStr(expected_tk)
                        << " at " << getPosition(m_tokenStart);
            throw new RuntimeError(errorString.str());
        }
        getNextToken();
    }

    std::string Lexer::getTokenStr(int token)
    {
        if((token > 32) && (token < 128))
        {
            char buf[4] = "' '";
            buf[1] = (char)token;
            return buf;
        }
        switch(token)
        {
            case LEX_EOF :
                return "EOF";
            case LEX_ID :
                return "ID";
            case LEX_INT :
                return "INT";
            case LEX_FLOAT :
                return "FLOAT";
            case LEX_STR :
                return "STRING";
            case LEX_EQUAL :
                return "==";
            case LEX_TYPEEQUAL :
                return "===";
            case LEX_NEQUAL :
                return "!=";
            case LEX_NTYPEEQUAL :
                return "!==";
            case LEX_LEQUAL :
                return "<=";
            case LEX_LSHIFT :
                return "<<";
            case LEX_LSHIFTEQUAL :
                return "<<=";
            case LEX_GEQUAL :
                return ">=";
            case LEX_RSHIFT :
                return ">>";
            case LEX_RSHIFTUNSIGNED :
                return ">>";
            case LEX_RSHIFTEQUAL :
                return ">>=";
            case LEX_PLUSEQUAL :
                return "+=";
            case LEX_MINUSEQUAL :
                return "-=";
            case LEX_PLUSPLUS :
                return "++";
            case LEX_MINUSMINUS :
                return "--";
            case LEX_ANDEQUAL :
                return "&=";
            case LEX_ANDAND :
                return "&&";
            case LEX_OREQUAL :
                return "|=";
            case LEX_OROR :
                return "||";
            case LEX_XOREQUAL :
                return "^=";
                // reserved words
            case LEX_R_IF :
                return "if";
            case LEX_R_ELSE :
                return "else";
            case LEX_R_DO :
                return "do";
            case LEX_R_WHILE :
                return "while";
            case LEX_R_FOR :
                return "for";
            case LEX_R_BREAK :
                return "break";
            case LEX_R_CONTINUE :
                return "continue";
            case LEX_R_FUNCTION :
                return "function";
            case LEX_R_RETURN :
                return "return";
            case LEX_R_VAR :
                return "var";
            case LEX_R_TRUE :
                return "true";
            case LEX_R_FALSE :
                return "false";
            case LEX_R_NULL :
                return "null";
            case LEX_R_UNDEFINED :
                return "undefined";
            case LEX_R_NEW :
                return "new";
        }
        std::stringstream msg;
        msg << "?[" << token << "]";
        return msg.str();
    }

    void Lexer::getNextCh()
    {
        m_currCh = m_nextCh;
        if(m_dataPos < m_dataEnd)
        {
            m_nextCh = m_data[m_dataPos];
        }
        else
        {
            m_nextCh = 0;
        }
        m_dataPos++;
    }

    void Lexer::getNextToken()
    {
        m_tk = LEX_EOF;
        m_tkStr.clear();
        while(m_currCh && isWhitespace(m_currCh))
        {
            getNextCh();
        }
        // newline comments
        if((m_currCh == '/') && (m_nextCh == '/'))
        {
            while(m_currCh && (m_currCh != '\n'))
            {
                getNextCh();
            }
            getNextCh();
            getNextToken();
            return;
        }
        // block comments
        if((m_currCh == '/') && (m_nextCh == '*'))
        {
            while(m_currCh && ((m_currCh != '*') || (m_nextCh != '/')))
            {
                getNextCh();
            }
            getNextCh();
            getNextCh();
            getNextToken();
            return;
        }
        // record beginning of this token
        m_tokenStart = (m_dataPos - 2);
        // tokens
        if(isAlpha(m_currCh))    //  IDs
        {
            while(isAlpha(m_currCh) || isNumeric(m_currCh))
            {
                m_tkStr += m_currCh;
                getNextCh();
            }
            m_tk = LEX_ID;
            if(m_tkStr == "if")
            {
                m_tk = LEX_R_IF;
            }
            else if(m_tkStr == "else")
            {
                m_tk = LEX_R_ELSE;
            }
            else if(m_tkStr == "do")
            {
                m_tk = LEX_R_DO;
            }
            else if(m_tkStr == "while")
            {
                m_tk = LEX_R_WHILE;
            }
            else if(m_tkStr == "for")
            {
                m_tk = LEX_R_FOR;
            }
            else if(m_tkStr == "break")
            {
                m_tk = LEX_R_BREAK;
            }
            else if(m_tkStr == "continue")
            {
                m_tk = LEX_R_CONTINUE;
            }
            else if(m_tkStr == "function")
            {
                m_tk = LEX_R_FUNCTION;
            }
            else if(m_tkStr == "return")
            {
                m_tk = LEX_R_RETURN;
            }
            else if(m_tkStr == "var")
            {
                m_tk = LEX_R_VAR;
            }
            else if(m_tkStr == "true")
            {
                m_tk = LEX_R_TRUE;
            }
            else if(m_tkStr == "false")
            {
                m_tk = LEX_R_FALSE;
            }
            else if(m_tkStr == "null")
            {
                m_tk = LEX_R_NULL;
            }
            else if(m_tkStr == "undefined")
            {
                m_tk = LEX_R_UNDEFINED;
            }
            else if(m_tkStr == "new")
            {
                m_tk = LEX_R_NEW;
            }
        }
        else if(isNumeric(m_currCh))      // Numbers
        {
            bool isHex = false;
            if(m_currCh == '0')
            {
                m_tkStr += m_currCh;
                getNextCh();
            }
            if(m_currCh == 'x')
            {
                isHex = true;
                m_tkStr += m_currCh;
                getNextCh();
            }
            m_tk = LEX_INT;
            while(isNumeric(m_currCh) || (isHex && isHexadecimal(m_currCh)))
            {
                m_tkStr += m_currCh;
                getNextCh();
            }
            if(!isHex && (m_currCh == '.'))
            {
                m_tk = LEX_FLOAT;
                m_tkStr += '.';
                getNextCh();
                while(isNumeric(m_currCh))
                {
                    m_tkStr += m_currCh;
                    getNextCh();
                }
            }
            // do fancy e-style floating point
            if(!isHex && ((m_currCh == 'e') || (m_currCh == 'E')))
            {
                m_tk = LEX_FLOAT;
                m_tkStr += m_currCh;
                getNextCh();
                if(m_currCh == '-')
                {
                    m_tkStr += m_currCh;
                    getNextCh();
                }
                while(isNumeric(m_currCh))
                {
                    m_tkStr += m_currCh;
                    getNextCh();
                }
            }
        }
        else if(m_currCh == '"')
        {
            // strings...
            getNextCh();
            while(m_currCh && (m_currCh != '"'))
            {
                if(m_currCh == '\\')
                {
                    getNextCh();
                    switch(m_currCh)
                    {
                        case 'n' :
                            m_tkStr += '\n';
                            break;
                        case '"' :
                            m_tkStr += '"';
                            break;
                        case '\\' :
                            m_tkStr += '\\';
                            break;
                        default:
                            m_tkStr += m_currCh;
                    }
                }
                else
                {
                    m_tkStr += m_currCh;
                }
                getNextCh();
            }
            getNextCh();
            m_tk = LEX_STR;
        }
        else if(m_currCh == '\'')
        {
            // strings again...
            getNextCh();
            while(m_currCh && (m_currCh != '\''))
            {
                if(m_currCh == '\\')
                {
                    getNextCh();
                    switch(m_currCh)
                    {
                        case 'n' :
                            m_tkStr += '\n';
                            break;
                        case 'a' :
                            m_tkStr += '\a';
                            break;
                        case 'r' :
                            m_tkStr += '\r';
                            break;
                        case 't' :
                            m_tkStr += '\t';
                            break;
                        case '\'' :
                            m_tkStr += '\'';
                            break;
                        case '\\' :
                            m_tkStr += '\\';
                            break;
                        case 'x' :   // hex digits
                            {
                                char buf[3] = "??";
                                getNextCh();
                                buf[0] = m_currCh;
                                getNextCh();
                                buf[1] = m_currCh;
                                m_tkStr += (char)strtol(buf,0,16);
                            }
                            break;
                        default:
                            if((m_currCh >= '0') && (m_currCh <= '7'))
                            {
                                // octal digits
                                char buf[4] = "???";
                                buf[0] = m_currCh;
                                getNextCh();
                                buf[1] = m_currCh;
                                getNextCh();
                                buf[2] = m_currCh;
                                m_tkStr += (char)strtol(buf,0,8);
                            }
                            else
                            {
                                m_tkStr += m_currCh;
                            }
                    }
                }
                else
                {
                    m_tkStr += m_currCh;
                }
                getNextCh();
            }
            getNextCh();
            m_tk = LEX_STR;
        }
        else
        {
            // single chars
            m_tk = m_currCh;
            if(m_currCh)
            {
                getNextCh();
            }
            if((m_tk == '=') && (m_currCh == '='))    // ==
            {
                m_tk = LEX_EQUAL;
                getNextCh();
                if(m_currCh == '=')    // ===
                {
                    m_tk = LEX_TYPEEQUAL;
                    getNextCh();
                }
            }
            else if((m_tk == '!') && (m_currCh == '='))      // !=
            {
                m_tk = LEX_NEQUAL;
                getNextCh();
                if(m_currCh == '=')    // !==
                {
                    m_tk = LEX_NTYPEEQUAL;
                    getNextCh();
                }
            }
            else if((m_tk == '<') && (m_currCh == '='))
            {
                m_tk = LEX_LEQUAL;
                getNextCh();
            }
            else if((m_tk == '<') && (m_currCh == '<'))
            {
                m_tk = LEX_LSHIFT;
                getNextCh();
                if(m_currCh == '=')    // <<=
                {
                    m_tk = LEX_LSHIFTEQUAL;
                    getNextCh();
                }
            }
            else if((m_tk == '>') && (m_currCh == '='))
            {
                m_tk = LEX_GEQUAL;
                getNextCh();
            }
            else if((m_tk == '>') && (m_currCh == '>'))
            {
                m_tk = LEX_RSHIFT;
                getNextCh();
                if(m_currCh == '=')    // >>=
                {
                    m_tk = LEX_RSHIFTEQUAL;
                    getNextCh();
                }
                else if(m_currCh == '>')      // >>>
                {
                    m_tk = LEX_RSHIFTUNSIGNED;
                    getNextCh();
                }
            }
            else if((m_tk == '+') && (m_currCh == '='))
            {
                m_tk = LEX_PLUSEQUAL;
                getNextCh();
            }
            else if((m_tk == '-') && (m_currCh == '='))
            {
                m_tk = LEX_MINUSEQUAL;
                getNextCh();
            }
            else if((m_tk == '+') && (m_currCh == '+'))
            {
                m_tk = LEX_PLUSPLUS;
                getNextCh();
            }
            else if((m_tk == '-') && (m_currCh == '-'))
            {
                m_tk = LEX_MINUSMINUS;
                getNextCh();
            }
            else if((m_tk == '&') && (m_currCh == '='))
            {
                m_tk = LEX_ANDEQUAL;
                getNextCh();
            }
            else if((m_tk == '&') && (m_currCh == '&'))
            {
                m_tk = LEX_ANDAND;
                getNextCh();
            }
            else if((m_tk == '|') && (m_currCh == '='))
            {
                m_tk = LEX_OREQUAL;
                getNextCh();
            }
            else if((m_tk == '|') && (m_currCh == '|'))
            {
                m_tk = LEX_OROR;
                getNextCh();
            }
            else if((m_tk == '^') && (m_currCh == '='))
            {
                m_tk = LEX_XOREQUAL;
                getNextCh();
            }
        }
        /* This isn't quite right yet */
        m_tokenLastEnd = m_tokenEnd;
        m_tokenEnd = (m_dataPos - 3);
    }

    std::string Lexer::getSubString(int lastPosition)
    {
        int lastCharIdx = (m_tokenLastEnd + 1);
        if(lastCharIdx < m_dataEnd)
        {
            /* save a memory alloc by using our data array to create the string */
            char old = m_data[lastCharIdx];
            m_data[lastCharIdx] = 0;
            std::string value = &m_data[lastPosition];
            m_data[lastCharIdx] = old;
            return value;
        }
        else
        {
            return std::string(&m_data[lastPosition]);
        }
    }

    Lexer* Lexer::getSubLex(int lastPosition)
    {
        int lastCharIdx = (m_tokenLastEnd + 1);
        if(lastCharIdx < m_dataEnd)
        {
            return new Lexer(this, lastPosition, lastCharIdx);
        }
        else
        {
            return new Lexer(this, lastPosition, m_dataEnd);
        }
    }

    std::string Lexer::getPosition(int pos)
    {
        int line;
        int col;
        if(pos < 0)
        {
            pos = m_tokenLastEnd;
        }
        line = 1;
        col = 1;
        for(int i=0; i<pos; i++)
        {
            char ch;
            if(i < m_dataEnd)
            {
                ch = m_data[i];
            }
            else
            {
                ch = 0;
            }
            col++;
            if(ch == '\n')
            {
                line++;
                col = 0;
            }
        }
        char buf[256];
        sprintf_s(buf, 256, "(line: %d, col: %d)", line, col);
        return buf;
    }
}