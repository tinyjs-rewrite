/*
 * TinyJS
 *
 * A single-file Javascript-alike engine
 *
 * Authored By Gordon Williams <gw@pur3.co.uk>
 *
 * Copyright (C) 2009 Pur3 Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * This is a simple program showing how to use TinyJS
 */

#include <assert.h>
#include <stdio.h>
#include "tinyjs.h"

const char *code =
    "function myfunc(x, y)"
    "{"
        "return x + y;"
    "}"
    "var a = myfunc(1,2);"
    "print(a);"
;

void js_print(TinyJS::Variable *v, void *userdata)
{
    (void)userdata;
    printf("> %s\n", v->getParameter("text")->getString().c_str());
}

void js_dump(TinyJS::Variable *v, void *userdata)
{
    (void)v;
    auto js = (TinyJS::Interpreter*)userdata;
    js->getRoot()->trace(">  ");
}

void js_lexer(TinyJS::Variable* v, void* userdata)
{
    
}


int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    auto js = new TinyJS::Interpreter();
    /* add the functions from TinyJS_Functions.cpp */
    registerFunctions(js);
    /* Add a native function */
    js->addNative("function print(text)", &js_print, 0);
    js->addNative("function dump()", &js_dump, js);
    /*
    * Execute out bit of code - we could call 'evaluate' here if
    * we wanted something returned
    */
    js->execute("var lets_quit = 0; function quit() { lets_quit = 1; }");
    printf(
        "Interactive mode...\n"
        "Type quit(); to exit, or print(...); to print something, or dump() to dump the symbol table!"
    );
    while (js->evaluate("lets_quit") == "0")
    {
        char buffer[2048];
        fgets(buffer, sizeof(buffer), stdin);
        try
        {
            js->execute(buffer);
        }
        catch(TinyJS::RuntimeError *e)
        {
          printf("ERROR: %s\n", e->text.c_str());
        }
    }
    delete js;
    return 0;
}
