/*
 * TinyJS
 *
 * A single-file Javascript-alike engine
 *
 * Authored By Gordon Williams <gw@pur3.co.uk>
 *
 * Copyright (C) 2009 Pur3 Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <assert.h>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include "tinyjs.h"

#define DEBUG_MEMORY 0

#ifdef _WIN32
    #ifdef _DEBUG
        #ifndef DBG_NEW
            #define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
            #define new DBG_NEW
        #endif
    #endif
#endif

#ifdef __GNUC__
    #define vsprintf_s vsnprintf
    #define sprintf_s snprintf
    #define _strdup strdup
#endif


#define ASSERT(X) \
    assert(X)



/* Frees the given link IF it isn't owned by anything else */
#define CLEAN(x) \
    { \
        VarLink* __v = x; \
        if(__v && !__v->owned) \
        { \
            delete __v; \
        } \
    }


/*
* Create a LINK to point to VAR and free the old link.
* BUT this is more clever - it tries to keep the old link if it's
* not owned to save allocations
*/

#define CREATE_LINK(LINK, VAR) \
    { \
        if(!LINK || LINK->owned) \
        { \
            LINK = new VarLink(VAR); \
        } \
        else \
        { \
            LINK->replaceWith(VAR); \
        } \
    }

#define __this_is_not_used_by_anyone 0

// ----------------------------------------------------------------------------------- Utils

bool isWhitespace(char ch);
bool isNumeric(char ch);
bool isNumber(const std::string& str);
bool isHexadecimal(char ch);
bool isAlpha(char ch);
bool isIDString(const char* s);
void replace(std::string& str, char textFrom, const char* textTo);
/// convert the given std::string into a quoted std::string suitable for javascript
std::string getJSString(const std::string& str);
/** Is the std::string alphanumeric */
bool isAlphaNum(const std::string& str);
char* strncopy(const char* instr, size_t n);


