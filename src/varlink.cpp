
#include "private.h"

namespace TinyJS
{
    VarLink::VarLink(Variable* nvar, const std::string& nname)
    {
    #if DEBUG_MEMORY
        mark_allocated(this);
    #endif
        this->name = nname;
        this->nextSibling = 0;
        this->prevSibling = 0;
        this->var = nvar->ref();
        this->owned = false;
    }

    VarLink::VarLink(const VarLink& link)
    {
        // Copy constructor
    #if DEBUG_MEMORY
        mark_allocated(this);
    #endif
        this->name = link.name;
        this->nextSibling = 0;
        this->prevSibling = 0;
        this->var = link.var->ref();
        this->owned = false;
    }

    VarLink::~VarLink()
    {
    #if DEBUG_MEMORY
        mark_deallocated(this);
    #endif
        var->unref();
    }

    void VarLink::replaceWith(Variable* newVar)
    {
        Variable* oldVar = var;
        var = newVar->ref();
        oldVar->unref();
    }

    void VarLink::replaceWith(VarLink* newVar)
    {
        if(newVar)
        {
            replaceWith(newVar->var);
        }
        else
        {
            replaceWith(new Variable());
        }
    }

    int VarLink::getIntName()
    {
        return atoi(name.c_str());
    }

    void VarLink::setIntName(int n)
    {
        char sIdx[64];
        sprintf_s(sIdx, sizeof(sIdx), "%d", n);
        name = sIdx;
    }
}

