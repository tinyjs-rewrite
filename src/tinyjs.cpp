/*
 * TinyJS
 *
 * A single-file Javascript-alike engine
 *
 * Authored By Gordon Williams <gw@pur3.co.uk>
 *
 * Copyright (C) 2009 Pur3 Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



/*
    Version 0.1:
        (gw) First published on Google Code

    Version 0.11: 
        Making sure the 'm_roottable' variable never changes
        'symbol_base' added for the current base of the sybmbol table

    Version 0.12: 
        Added findChildOrCreate, changed std::string passing to use references
        Fixed broken std::string encoding in getJSString()
        Removed getInitCode and added getJSON instead
        Added nil
        Added rough JSON parsing
        Improved example app

    Version 0.13:
        Added tokenEnd/tokenLastEnd to lexer to avoid parsing whitespace
        Ability to define functions without names
        Can now do "var mine = function(a,b) { ... };"
        Slightly better 'trace' function
        Added findChildOrCreateByPath function
        Added simple test suite
        Added skipping of blocks when not executing

    Version 0.14:
        Added parsing of more number types
        Added parsing of std::string defined with '
        Changed nil to null as per spec, added 'undefined'
        Now set variables with the correct scope, and treat unknown as 'undefined' rather than failing
        Added proper (I hope) handling of null and undefined
        Added check for '==='

    Version 0.15: 
        Fix for possible memory leaks

    Version 0.16: 
        Removal of un-needed findRecursive calls
        symbol_base removed and replaced with 'm_scopes' stack
        Added reference counting a proper tree structure
        (Allowing pass by reference)
        Allowed JSON output to output IDs, not std::strings
        Added get/set for array indices
        Changed Callbacks to include user data pointer
        Added some support for objects
        Added more Java-esque builtin functions

    Version 0.17: 
        Now we don't deepCopy the parent object of the class
        Added JSON.std::stringify and eval()
        Nicer JSON indenting
        Fixed function output in JSON
        Added evaluateComplex
        Fixed some reentrancy issues with evaluate/execute

    Version 0.18:
        Fixed some issues with code being executed when it shouldn't

    Version 0.19:
        Added array.length
        Changed '__parent' to 'prototype' to bring it more in line with javascript

    Version 0.20:
        Added '%' operator

    Version 0.21:
        Added array type
        String.length() no more - now String.length
        Added extra constructors to reduce confusion
        Fixed checks against undefined

    Version 0.22:
        First part of ardi's changes:
            sprintf -> sprintf_s
            extra tokens parsed
            array memory leak fixed
            Fixed memory leak in evaluateComplex
            Fixed memory leak in FOR loops
            Fixed memory leak for unary minus

    Version 0.23:
        Allowed evaluate[Complex] to take in semi-colon separated
        statements and then only return the value from the last one.
        Also checks to make sure *everything* was parsed.
        Ints + doubles are now stored in binary form (faster + more precise)

    Version 0.24:
        More useful error for maths ops
        Don't dump everything on a match error.

    Version 0.25:
        Better std::string escaping

    Version 0.26:
        Add Variable::equals
        Add built-in array functions

    Version 0.27:
        Added OZLB's TinyJS.setVariable (with some tweaks)
        Added OZLB's Maths Functions

    Version 0.28:
        Ternary operator
        Rudimentary call stack on error
        Added String Character functions
        Added shift operators

    Version 0.29:
        Added new object via functions
        Fixed getString() for double on some platforms

    Version 0.30:
        Rlyeh Mario's patch for Math Functions on VC++

    Version 0.31: 
        Add exec() to TinyJS functions
        Now print quoted JSON that can be read by PHP/Python parsers
        Fixed postfix increment operator

    Version 0.32:
        Fixed Math.randInt on 32 bit PCs, where it was broken

    Version 0.33:
        Fixed Memory leak + brokenness on === comparison


    NOTE:
        Constructing an array with an initial length 'Array(5)' doesn't work
        Recursive loops of data such as a.foo = a; fail to be garbage collected
        length variable cannot be set
        The postfix increment operator returns the current value, not the previous as it should.
        There is no prefix increment operator
        Arrays are implemented as a linked list - hence a lookup time is O(n)

    TODO:
        Utility va-args style function in TinyJS for executing a function directly
        Merge the parsing of expressions/statements so eval("statement") works like we'd expect.
        Move 'shift' implementation into mathsOp
*/


#include "private.h"

namespace TinyJS
{
    #if DEBUG_MEMORY
    std::vector<Variable*> allocatedVars;
    std::vector<VarLink*> allocatedLinks;

    void mark_allocated(Variable* v)
    {
        allocatedVars.push_back(v);
    }

    void mark_deallocated(Variable* v)
    {
        for(size_t i=0; i<allocatedVars.size(); i++)
        {
            if(allocatedVars[i] == v)
            {
                allocatedVars.erase(allocatedVars.begin()+i);
                break;
            }
        }
    }

    void mark_allocated(VarLink* v)
    {
        allocatedLinks.push_back(v);
    }

    void mark_deallocated(VarLink* v)
    {
        for(size_t i=0; i<allocatedLinks.size(); i++)
        {
            if(allocatedLinks[i] == v)
            {
                allocatedLinks.erase(allocatedLinks.begin()+i);
                break;
            }
        }
    }

    void show_allocated()
    {
        for(size_t i=0; i<allocatedVars.size(); i++)
        {
            printf("ALLOCATED, %d refs\n", allocatedVars[i]->getRefs());
            allocatedVars[i]->trace("  ");
        }
        for(size_t i=0; i<allocatedLinks.size(); i++)
        {
            printf("ALLOCATED LINK %s, allocated[%d] to \n", allocatedLinks[i]->name.c_str(), allocatedLinks[i]->var->getRefs());
            allocatedLinks[i]->var->trace("  ");
        }
        allocatedVars.clear();
        allocatedLinks.clear();
    }
    #endif

    Interpreter::Interpreter()
    {
        m_lexer = 0;
        m_roottable = (new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT))->ref();
        // Add built-in classes
        m_stringclass = (new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT))->ref();
        m_arrayclass = (new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT))->ref();
        m_objectclass = (new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT))->ref();
        m_roottable->addChild("String", m_stringclass);
        m_roottable->addChild("Array", m_arrayclass);
        m_roottable->addChild("Object", m_objectclass);
    }

    Interpreter::~Interpreter()
    {
        ASSERT(!m_lexer);
        m_scopes.clear();
        m_stringclass->unref();
        m_arrayclass->unref();
        m_objectclass->unref();
        m_roottable->unref();
    #if DEBUG_MEMORY
        show_allocated();
    #endif
    }

    void Interpreter::trace()
    {
        m_roottable->trace();
    }

    void Interpreter::execute(const std::string& code)
    {
        Lexer* oldLex = m_lexer;
        std::vector<Variable*> oldScopes = m_scopes;
        m_lexer = new Lexer(code);
    #ifdef TINYJS_CALL_STACK
        m_callstack.clear();
    #endif
        m_scopes.clear();
        m_scopes.push_back(m_roottable);
        try
        {
            bool execute = true;
            while(m_lexer->m_tk)
            {
                statement(execute);
            }
        }
        catch(RuntimeError* e)
        {
            std::stringstream msg;
            msg << "Error " << e->text;
    #ifdef TINYJS_CALL_STACK
            for(int i=int(m_callstack.size())-1; i>=0; i--)
            {
                msg << "\n" << i << ": " << m_callstack.at(size_t(i));
            }
    #endif
            msg << " at " << m_lexer->getPosition();
            delete m_lexer;
            m_lexer = oldLex;
            throw new RuntimeError(msg.str());
        }
        delete m_lexer;
        m_lexer = oldLex;
        m_scopes = oldScopes;
    }

    VarLink Interpreter::evaluateComplex(const std::string& code)
    {
        Lexer* oldLex = m_lexer;
        std::vector<Variable*> oldScopes = m_scopes;
        m_lexer = new Lexer(code);
    #ifdef TINYJS_CALL_STACK
        m_callstack.clear();
    #endif
        m_scopes.clear();
        m_scopes.push_back(m_roottable);
        VarLink* v = 0;
        try
        {
            bool execute = true;
            do
            {
                CLEAN(v);
                v = base(execute);
                if(m_lexer->m_tk!=LEX_EOF)
                {
                    m_lexer->match(';');
                }
            }
            while(m_lexer->m_tk!=LEX_EOF);
        }
        catch(RuntimeError* e)
        {
            std::stringstream msg;
            msg << "Error " << e->text;
    #ifdef TINYJS_CALL_STACK
            for(int i=int(m_callstack.size())-1; i>=0; i--)
            {
                msg << "\n" << i << ": " << m_callstack.at(size_t(i));
            }
    #endif
            msg << " at " << m_lexer->getPosition();
            delete m_lexer;
            m_lexer = oldLex;
            throw new RuntimeError(msg.str());
        }
        delete m_lexer;
        m_lexer = oldLex;
        m_scopes = oldScopes;
        if(v)
        {
            VarLink r = *v;
            CLEAN(v);
            return r;
        }
        // return undefined...
        return VarLink(new Variable());
    }

    std::string Interpreter::evaluate(const std::string& code)
    {
        return evaluateComplex(code).var->getString();
    }

    void Interpreter::parseFunctionArguments(Variable* funcVar)
    {
        m_lexer->match('(');
        while(m_lexer->m_tk != ')')
        {
            funcVar->addChildNoDup(m_lexer->m_tkStr);
            m_lexer->match(LEX_ID);
            if(m_lexer->m_tk != ')')
            {
                m_lexer->match(',');
            }
        }
        m_lexer->match(')');
    }

    void Interpreter::addNative(const std::string& funcDesc, JSCallback ptr, void* userdata)
    {
        Lexer* oldLex = m_lexer;
        m_lexer = new Lexer(funcDesc);
        Variable* base = m_roottable;
        m_lexer->match(LEX_R_FUNCTION);
        std::string funcName = m_lexer->m_tkStr;
        m_lexer->match(LEX_ID);
        /* Check for dots, we might want to do something like function String.substd::string ... */
        while(m_lexer->m_tk == '.')
        {
            m_lexer->match('.');
            VarLink* link = base->findChild(funcName);
            // if it doesn't exist, make an object class
            if(!link)
            {
                link = base->addChild(funcName, new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT));
            }
            base = link->var;
            funcName = m_lexer->m_tkStr;
            m_lexer->match(LEX_ID);
        }
        Variable* funcVar = new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_FUNCTION | SCRIPTVAR_NATIVE);
        funcVar->setCallback(ptr, userdata);
        parseFunctionArguments(funcVar);
        delete m_lexer;
        m_lexer = oldLex;
        base->addChild(funcName, funcVar);
    }

    VarLink* Interpreter::parseFunctionDefinition()
    {
        // actually parse a function...
        m_lexer->match(LEX_R_FUNCTION);
        std::string funcName = TINYJS_TEMP_NAME;
        /* we can have functions without names */
        if(m_lexer->m_tk==LEX_ID)
        {
            funcName = m_lexer->m_tkStr;
            m_lexer->match(LEX_ID);
        }
        VarLink* funcVar = new VarLink(new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_FUNCTION), funcName);
        parseFunctionArguments(funcVar->var);
        int funcBegin = m_lexer->m_tokenStart;
        bool noexecute = false;
        block(noexecute);
        funcVar->var->data = m_lexer->getSubString(funcBegin);
        return funcVar;
    }

    /*
    * Handle a function call (assumes we've parsed the function name and we're
    * on the start bracket). 'parent' is the object that contains this method,
    * if there was one (otherwise it's just a normnal function).
    */
    VarLink* Interpreter::functionCall(bool& execute, VarLink* function, Variable* parent)
    {
        if(execute)
        {
            if(!function->var->isFunction())
            {
                std::string errorMsg = "Expecting '";
                errorMsg = errorMsg + function->name + "' to be a function";
                throw new RuntimeError(errorMsg.c_str());
            }
            m_lexer->match('(');
            // create a new symbol table entry for execution of this function
            Variable* functionRoot = new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_FUNCTION);
            if(parent)
            {
                functionRoot->addChildNoDup("this", parent);
            }
            // grab in all parameters
            VarLink* v = function->var->firstChild;
            while(v)
            {
                VarLink* value = base(execute);
                if(execute)
                {
                    if(value->var->isBasic())
                    {
                        // pass by value
                        functionRoot->addChild(v->name, value->var->deepCopy());
                    }
                    else
                    {
                        // pass by reference
                        functionRoot->addChild(v->name, value->var);
                    }
                }
                CLEAN(value);
                if(m_lexer->m_tk!=')')
                {
                    m_lexer->match(',');
                }
                v = v->nextSibling;
            }
            m_lexer->match(')');
            // setup a return variable
            VarLink* returnVar = NULL;
            // execute function!
            // add the function's execute space to the symbol table so we can recurse
            VarLink* returnVarLink = functionRoot->addChild(TINYJS_RETURN_VAR);
            m_scopes.push_back(functionRoot);
            #ifdef TINYJS_CALL_STACK
            m_callstack.push_back(function->name + " from " + m_lexer->getPosition());
            #endif
            if(function->var->isNative())
            {
                ASSERT(function->var->jsCallback);
                function->var->jsCallback(functionRoot, function->var->jsCallbackUserData);
            }
            else
            {
                /*
                * we just want to execute the block, but something could
                * have messed up and left us with the wrong ScriptLex, so
                * we want to be careful here...
                */
                RuntimeError* exception = 0;
                Lexer* oldLex = m_lexer;
                Lexer* newLex = new Lexer(function->var->getString());
                m_lexer = newLex;
                try
                {
                    block(execute);
                    // because return will probably have called this, and set execute to false
                    execute = true;
                }
                catch(RuntimeError* e)
                {
                    exception = e;
                }
                delete newLex;
                m_lexer = oldLex;
                if(exception)
                {
                    throw exception;
                }
            }
            #ifdef TINYJS_CALL_STACK
            if(!m_callstack.empty())
            {
                m_callstack.pop_back();
            }
            #endif
            m_scopes.pop_back();
            /* get the real return var before we remove it from our function */
            returnVar = new VarLink(returnVarLink->var);
            functionRoot->removeLink(returnVarLink);
            delete functionRoot;
            if(returnVar)
            {
                return returnVar;
            }
            else
            {
                return new VarLink(new Variable());
            }
        }
        else
        {
            // function, but not executing - just parse args and be done
            m_lexer->match('(');
            while(m_lexer->m_tk != ')')
            {
                VarLink* value = base(execute);
                CLEAN(value);
                if(m_lexer->m_tk!=')')
                {
                    m_lexer->match(',');
                }
            }
            m_lexer->match(')');
            if(m_lexer->m_tk == '{')    // TODO: why is this here?
            {
                block(execute);
            }
            /*
            * function will be a blank scriptvarlink if we're not executing,
            * so just return it rather than an alloc/free
            */
            return function;
        }
    }

    VarLink* Interpreter::factor(bool& execute)
    {
        if(m_lexer->m_tk=='(')
        {
            m_lexer->match('(');
            VarLink* a = base(execute);
            m_lexer->match(')');
            return a;
        }
        if(m_lexer->m_tk==LEX_R_TRUE)
        {
            m_lexer->match(LEX_R_TRUE);
            return new VarLink(new Variable(1));
        }
        if(m_lexer->m_tk==LEX_R_FALSE)
        {
            m_lexer->match(LEX_R_FALSE);
            return new VarLink(new Variable(0));
        }
        if(m_lexer->m_tk==LEX_R_NULL)
        {
            m_lexer->match(LEX_R_NULL);
            return new VarLink(new Variable(TINYJS_BLANK_DATA,SCRIPTVAR_NULL));
        }
        if(m_lexer->m_tk==LEX_R_UNDEFINED)
        {
            m_lexer->match(LEX_R_UNDEFINED);
            return new VarLink(new Variable(TINYJS_BLANK_DATA,SCRIPTVAR_UNDEFINED));
        }
        if(m_lexer->m_tk==LEX_ID)
        {
            VarLink* a = execute ? findInScopes(m_lexer->m_tkStr) : new VarLink(new Variable());
            //printf("0x%08X for %s at %s\n", (unsigned int)a, m_lexer->m_tkStr.c_str(), m_lexer->m_getPosition().c_str());
            /* The parent if we're executing a method call */
            Variable* parent = 0;
            if(execute && !a)
            {
                /* Variable doesn't exist! JavaScript says we should create it

                 * (we won't add it here. This is done in the assignment operator)*/
                a = new VarLink(new Variable(), m_lexer->m_tkStr);
            }
            m_lexer->match(LEX_ID);
            while(m_lexer->m_tk=='(' || m_lexer->m_tk=='.' || m_lexer->m_tk=='[')
            {
                if(m_lexer->m_tk=='(')    // ------------------------------------- Function Call
                {
                    a = functionCall(execute, a, parent);
                }
                else if(m_lexer->m_tk == '.')      // ------------------------------------- Record Access
                {
                    m_lexer->match('.');
                    if(execute)
                    {
                        const std::string& name = m_lexer->m_tkStr;
                        VarLink* child = a->var->findChild(name);
                        if(!child)
                        {
                            child = findInParentClasses(a->var, name);
                        }
                        if(!child)
                        {
                            /* if we haven't found this defined yet, use the built-in

                               'length' properly */
                            if(a->var->isArray() && name == "length")
                            {
                                int l = a->var->getArrayLength();
                                child = new VarLink(new Variable(l));
                            }
                            else if(a->var->isString() && name == "length")
                            {
                                int l = a->var->getString().size();
                                child = new VarLink(new Variable(l));
                            }
                            else
                            {
                                child = a->var->addChild(name);
                            }
                        }
                        parent = a->var;
                        a = child;
                    }
                    m_lexer->match(LEX_ID);
                }
                else if(m_lexer->m_tk == '[')      // ------------------------------------- Array Access
                {
                    m_lexer->match('[');
                    VarLink* index = base(execute);
                    m_lexer->match(']');
                    if(execute)
                    {
                        VarLink* child = a->var->findChildOrCreate(index->var->getString());
                        parent = a->var;
                        a = child;
                    }
                    CLEAN(index);
                }
                else
                {
                    ASSERT(0);
                }
            }
            return a;
        }
        if(m_lexer->m_tk==LEX_INT || m_lexer->m_tk==LEX_FLOAT)
        {
            Variable* a = new Variable(m_lexer->m_tkStr,
                                           ((m_lexer->m_tk==LEX_INT)?SCRIPTVAR_INTEGER:SCRIPTVAR_DOUBLE));
            m_lexer->match(m_lexer->m_tk);
            return new VarLink(a);
        }
        if(m_lexer->m_tk==LEX_STR)
        {
            Variable* a = new Variable(m_lexer->m_tkStr, SCRIPTVAR_STRING);
            m_lexer->match(LEX_STR);
            return new VarLink(a);
        }
        if(m_lexer->m_tk=='{')
        {
            Variable* contents = new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT);
            /* JSON-style object definition */
            m_lexer->match('{');
            while(m_lexer->m_tk != '}')
            {
                std::string id = m_lexer->m_tkStr;
                // we only allow std::strings or IDs on the left hand side of an initialisation
                if(m_lexer->m_tk==LEX_STR)
                {
                    m_lexer->match(LEX_STR);
                }
                else
                {
                    m_lexer->match(LEX_ID);
                }
                m_lexer->match(':');
                if(execute)
                {
                    VarLink* a = base(execute);
                    contents->addChild(id, a->var);
                    CLEAN(a);
                }
                // no need to clean here, as it will definitely be used
                if(m_lexer->m_tk != '}')
                {
                    m_lexer->match(',');
                }
            }
            m_lexer->match('}');
            return new VarLink(contents);
        }
        if(m_lexer->m_tk=='[')
        {
            Variable* contents = new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_ARRAY);
            /* JSON-style array */
            m_lexer->match('[');
            int idx = 0;
            while(m_lexer->m_tk != ']')
            {
                if(execute)
                {
                    char idx_str[16]; // big enough for 2^32
                    sprintf_s(idx_str, sizeof(idx_str), "%d",idx);
                    VarLink* a = base(execute);
                    contents->addChild(idx_str, a->var);
                    CLEAN(a);
                }
                // no need to clean here, as it will definitely be used
                if(m_lexer->m_tk != ']')
                {
                    m_lexer->match(',');
                }
                idx++;
            }
            m_lexer->match(']');
            return new VarLink(contents);
        }
        if(m_lexer->m_tk==LEX_R_FUNCTION)
        {
            VarLink* funcVar = parseFunctionDefinition();
            if(funcVar->name != TINYJS_TEMP_NAME)
            {
                TRACE("Functions not defined at statement-level are not meant to have a name");
            }
            return funcVar;
        }
        if(m_lexer->m_tk==LEX_R_NEW)
        {
            // new -> create a new object
            m_lexer->match(LEX_R_NEW);
            const std::string& className = m_lexer->m_tkStr;
            if(execute)
            {
                VarLink* objClassOrFunc = findInScopes(className);
                if(!objClassOrFunc)
                {
                    TRACE("%s is not a valid class name", className.c_str());
                    return new VarLink(new Variable());
                }
                m_lexer->match(LEX_ID);
                Variable* obj = new Variable(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT);
                VarLink* objLink = new VarLink(obj);
                if(objClassOrFunc->var->isFunction())
                {
                    CLEAN(functionCall(execute, objClassOrFunc, obj));
                }
                else
                {
                    obj->addChild(TINYJS_PROTOTYPE_CLASS, objClassOrFunc->var);
                    if(m_lexer->m_tk == '(')
                    {
                        m_lexer->match('(');
                        m_lexer->match(')');
                    }
                }
                return objLink;
            }
            else
            {
                m_lexer->match(LEX_ID);
                if(m_lexer->m_tk == '(')
                {
                    m_lexer->match('(');
                    m_lexer->match(')');
                }
            }
        }
        // Nothing we can do here... just hope it's the end...
        m_lexer->match(LEX_EOF);
        return 0;
    }

    VarLink* Interpreter::unary(bool& execute)
    {
        VarLink* a;
        if(m_lexer->m_tk=='!')
        {
            m_lexer->match('!'); // binary not
            a = factor(execute);
            if(execute)
            {
                Variable zero(0);
                Variable* res = a->var->mathsOp(&zero, LEX_EQUAL);
                CREATE_LINK(a, res);
            }
        }
        else
        {
            a = factor(execute);
        }
        return a;
    }

    VarLink* Interpreter::term(bool& execute)
    {
        VarLink* a = unary(execute);
        while(m_lexer->m_tk=='*' || m_lexer->m_tk=='/' || m_lexer->m_tk=='%')
        {
            int op = m_lexer->m_tk;
            m_lexer->match(m_lexer->m_tk);
            VarLink* b = unary(execute);
            if(execute)
            {
                Variable* res = a->var->mathsOp(b->var, op);
                CREATE_LINK(a, res);
            }
            CLEAN(b);
        }
        return a;
    }

    VarLink* Interpreter::expression(bool& execute)
    {
        bool negate = false;
        if(m_lexer->m_tk=='-')
        {
            m_lexer->match('-');
            negate = true;
        }
        VarLink* a = term(execute);
        if(negate)
        {
            Variable zero(0);
            Variable* res = zero.mathsOp(a->var, '-');
            CREATE_LINK(a, res);
        }
        while(m_lexer->m_tk=='+' || m_lexer->m_tk=='-' ||
                m_lexer->m_tk==LEX_PLUSPLUS || m_lexer->m_tk==LEX_MINUSMINUS)
        {
            int op = m_lexer->m_tk;
            m_lexer->match(m_lexer->m_tk);
            if(op==LEX_PLUSPLUS || op==LEX_MINUSMINUS)
            {
                if(execute)
                {
                    Variable one(1);
                    Variable* res = a->var->mathsOp(&one, op==LEX_PLUSPLUS ? '+' : '-');
                    VarLink* oldValue = new VarLink(a->var);
                    // in-place add/subtract
                    a->replaceWith(res);
                    CLEAN(a);
                    a = oldValue;
                }
            }
            else
            {
                VarLink* b = term(execute);
                if(execute)
                {
                    // not in-place, so just replace
                    Variable* res = a->var->mathsOp(b->var, op);
                    CREATE_LINK(a, res);
                }
                CLEAN(b);
            }
        }
        return a;
    }

    VarLink* Interpreter::shift(bool& execute)
    {
        VarLink* a = expression(execute);
        if(m_lexer->m_tk==LEX_LSHIFT || m_lexer->m_tk==LEX_RSHIFT || m_lexer->m_tk==LEX_RSHIFTUNSIGNED)
        {
            int op = m_lexer->m_tk;
            m_lexer->match(op);
            VarLink* b = base(execute);
            int shift = execute ? b->var->getInt() : 0;
            CLEAN(b);
            if(execute)
            {
                if(op==LEX_LSHIFT)
                {
                    a->var->setInt(a->var->getInt() << shift);
                }
                if(op==LEX_RSHIFT)
                {
                    a->var->setInt(a->var->getInt() >> shift);
                }
                if(op==LEX_RSHIFTUNSIGNED)
                {
                    a->var->setInt(((unsigned int)a->var->getInt()) >> shift);
                }
            }
        }
        return a;
    }

    VarLink* Interpreter::condition(bool& execute)
    {
        VarLink* a = shift(execute);
        VarLink* b;
        while(m_lexer->m_tk==LEX_EQUAL || m_lexer->m_tk==LEX_NEQUAL ||
              m_lexer->m_tk==LEX_TYPEEQUAL || m_lexer->m_tk==LEX_NTYPEEQUAL ||
              m_lexer->m_tk==LEX_LEQUAL || m_lexer->m_tk==LEX_GEQUAL ||
              m_lexer->m_tk=='<' || m_lexer->m_tk=='>')
        {
            int op = m_lexer->m_tk;
            m_lexer->match(m_lexer->m_tk);
            b = shift(execute);
            if(execute)
            {
                Variable* res = a->var->mathsOp(b->var, op);
                CREATE_LINK(a,res);
            }
            CLEAN(b);
        }
        return a;
    }

    VarLink* Interpreter::logic(bool& execute)
    {
        VarLink* a = condition(execute);
        VarLink* b;
        while(m_lexer->m_tk=='&' || m_lexer->m_tk=='|' || m_lexer->m_tk=='^' || m_lexer->m_tk==LEX_ANDAND || m_lexer->m_tk==LEX_OROR)
        {
            bool noexecute = false;
            int op = m_lexer->m_tk;
            m_lexer->match(m_lexer->m_tk);
            bool shortCircuit = false;
            bool boolean = false;
            // if we have short-circuit ops, then if we know the outcome
            // we don't bother to execute the other op. Even if not
            // we need to tell mathsOp it's an & or |
            if(op==LEX_ANDAND)
            {
                op = '&';
                shortCircuit = !a->var->getBool();
                boolean = true;
            }
            else if(op==LEX_OROR)
            {
                op = '|';
                shortCircuit = a->var->getBool();
                boolean = true;
            }
            b = condition(shortCircuit ? noexecute : execute);
            if(execute && !shortCircuit)
            {
                if(boolean)
                {
                    Variable* newa = new Variable(a->var->getBool());
                    Variable* newb = new Variable(b->var->getBool());
                    CREATE_LINK(a, newa);
                    CREATE_LINK(b, newb);
                }
                Variable* res = a->var->mathsOp(b->var, op);
                CREATE_LINK(a, res);
            }
            CLEAN(b);
        }
        return a;
    }

    VarLink* Interpreter::ternary(bool& execute)
    {
        VarLink* lhs = logic(execute);
        bool noexec = false;
        if(m_lexer->m_tk=='?')
        {
            m_lexer->match('?');
            if(!execute)
            {
                CLEAN(lhs);
                CLEAN(base(noexec));
                m_lexer->match(':');
                CLEAN(base(noexec));
            }
            else
            {
                bool first = lhs->var->getBool();
                CLEAN(lhs);
                if(first)
                {
                    lhs = base(execute);
                    m_lexer->match(':');
                    CLEAN(base(noexec));
                }
                else
                {
                    CLEAN(base(noexec));
                    m_lexer->match(':');
                    lhs = base(execute);
                }
            }
        }
        return lhs;
    }

    VarLink* Interpreter::base(bool& execute)
    {
        VarLink* lhs = ternary(execute);
        if(m_lexer->m_tk=='=' || m_lexer->m_tk==LEX_PLUSEQUAL || m_lexer->m_tk==LEX_MINUSEQUAL)
        {
            /* If we're assigning to this and we don't have a parent,

             * add it to the symbol table m_roottable as per JavaScript. */
            if(execute && !lhs->owned)
            {
                if(lhs->name.length()>0)
                {
                    VarLink* realLhs = m_roottable->addChildNoDup(lhs->name, lhs->var);
                    CLEAN(lhs);
                    lhs = realLhs;
                }
                else
                {
                    TRACE("Trying to assign to an un-named type\n");
                }
            }
            int op = m_lexer->m_tk;
            m_lexer->match(m_lexer->m_tk);
            VarLink* rhs = base(execute);
            if(execute)
            {
                if(op=='=')
                {
                    lhs->replaceWith(rhs);
                }
                else if(op==LEX_PLUSEQUAL)
                {
                    Variable* res = lhs->var->mathsOp(rhs->var, '+');
                    lhs->replaceWith(res);
                }
                else if(op==LEX_MINUSEQUAL)
                {
                    Variable* res = lhs->var->mathsOp(rhs->var, '-');
                    lhs->replaceWith(res);
                }
                else
                {
                    ASSERT(0);
                }
            }
            CLEAN(rhs);
        }
        return lhs;
    }

    void Interpreter::block(bool& execute)
    {
        m_lexer->match('{');
        if(execute)
        {
            while(m_lexer->m_tk && m_lexer->m_tk!='}')
            {
                statement(execute);
            }
            m_lexer->match('}');
        }
        else
        {
            // fast skip of blocks
            int brackets = 1;
            while(m_lexer->m_tk && brackets)
            {
                if(m_lexer->m_tk == '{')
                {
                    brackets++;
                }
                if(m_lexer->m_tk == '}')
                {
                    brackets--;
                }
                m_lexer->match(m_lexer->m_tk);
            }
        }
    }

    void Interpreter::statement(bool& execute)
    {
        if(m_lexer->m_tk==LEX_ID ||
                m_lexer->m_tk==LEX_INT ||
                m_lexer->m_tk==LEX_FLOAT ||
                m_lexer->m_tk==LEX_STR ||
                m_lexer->m_tk=='-')
        {
            /* Execute a simple statement that only contains basic arithmetic... */
            CLEAN(base(execute));
            m_lexer->match(';');
        }
        else if(m_lexer->m_tk=='{')
        {
            /* A block of code */
            block(execute);
        }
        else if(m_lexer->m_tk==';')
        {
            /* Empty statement - to allow things like ;;; */
            m_lexer->match(';');
        }
        else if(m_lexer->m_tk==LEX_R_VAR)
        {
            /* variable creation. TODO - we need a better way of parsing the left

             * hand side. Maybe just have a flag called can_create_var that we

             * set and then we parse as if we're doing a normal equals.*/
            m_lexer->match(LEX_R_VAR);
            while(m_lexer->m_tk != ';')
            {
                VarLink* a = 0;
                if(execute)
                {
                    a = m_scopes.back()->findChildOrCreate(m_lexer->m_tkStr);
                }
                m_lexer->match(LEX_ID);
                // now do stuff defined with dots
                while(m_lexer->m_tk == '.')
                {
                    m_lexer->match('.');
                    if(execute)
                    {
                        VarLink* lastA = a;
                        a = lastA->var->findChildOrCreate(m_lexer->m_tkStr);
                    }
                    m_lexer->match(LEX_ID);
                }
                // sort out initialiser
                if(m_lexer->m_tk == '=')
                {
                    m_lexer->match('=');
                    VarLink* var = base(execute);
                    if(execute)
                    {
                        a->replaceWith(var);
                    }
                    CLEAN(var);
                }
                if(m_lexer->m_tk != ';')
                {
                    m_lexer->match(',');
                }
            }
            m_lexer->match(';');
        }
        else if(m_lexer->m_tk==LEX_R_IF)
        {
            m_lexer->match(LEX_R_IF);
            m_lexer->match('(');
            VarLink* var = base(execute);
            m_lexer->match(')');
            bool cond = execute && var->var->getBool();
            CLEAN(var);
            bool noexecute = false; // because we need to be abl;e to write to it
            statement(cond ? execute : noexecute);
            if(m_lexer->m_tk==LEX_R_ELSE)
            {
                m_lexer->match(LEX_R_ELSE);
                statement(cond ? noexecute : execute);
            }
        }
        else if(m_lexer->m_tk==LEX_R_WHILE)
        {
            // We do repetition by pulling out the std::string representing our statement
            // there's definitely some opportunity for optimisation here
            m_lexer->match(LEX_R_WHILE);
            m_lexer->match('(');
            int whileCondStart = m_lexer->m_tokenStart;
            bool noexecute = false;
            VarLink* cond = base(execute);
            bool loopCond = execute && cond->var->getBool();
            CLEAN(cond);
            Lexer* whileCond = m_lexer->getSubLex(whileCondStart);
            m_lexer->match(')');
            int whileBodyStart = m_lexer->m_tokenStart;
            statement(loopCond ? execute : noexecute);
            Lexer* whileBody = m_lexer->getSubLex(whileBodyStart);
            Lexer* oldLex = m_lexer;
            int loopCount = TINYJS_LOOP_MAX_ITERATIONS;
            while(loopCond && loopCount-->0)
            {
                whileCond->reset();
                m_lexer = whileCond;
                cond = base(execute);
                loopCond = execute && cond->var->getBool();
                CLEAN(cond);
                if(loopCond)
                {
                    whileBody->reset();
                    m_lexer = whileBody;
                    statement(execute);
                }
            }
            m_lexer = oldLex;
            delete whileCond;
            delete whileBody;
            if(loopCount<=0)
            {
                m_roottable->trace();
                TRACE("WHILE Loop exceeded %d iterations at %s\n",
                    TINYJS_LOOP_MAX_ITERATIONS, m_lexer->getPosition().c_str());
                throw new RuntimeError("LOOP_ERROR");
            }
        }
        else if(m_lexer->m_tk == LEX_R_FOR)
        {
            int loopCount;
            int forCondStart;
            int forIterStart;
            int forBodyStart;
            bool noexecute;
            bool loopCond;
            noexecute = false;
            m_lexer->match(LEX_R_FOR);
            m_lexer->match('(');
            statement(execute); // initialisation
            //m_lexer->match(';');
            forCondStart = m_lexer->m_tokenStart;
            VarLink* cond = base(execute); // condition
            loopCond = execute && cond->var->getBool();
            CLEAN(cond);
            Lexer* forCond = m_lexer->getSubLex(forCondStart);
            m_lexer->match(';');
            forIterStart = m_lexer->m_tokenStart;
            CLEAN(base(noexecute)); // iterator
            Lexer* forIter = m_lexer->getSubLex(forIterStart);
            m_lexer->match(')');
            forBodyStart = m_lexer->m_tokenStart;
            statement(loopCond ? execute : noexecute);
            Lexer* forBody = m_lexer->getSubLex(forBodyStart);
            Lexer* oldLex = m_lexer;
            if(loopCond)
            {
                forIter->reset();
                m_lexer = forIter;
                CLEAN(base(execute));
            }
            loopCount = TINYJS_LOOP_MAX_ITERATIONS;
            while(execute && loopCond && loopCount-->0)
            {
                forCond->reset();
                m_lexer = forCond;
                cond = base(execute);
                loopCond = cond->var->getBool();
                CLEAN(cond);
                if(execute && loopCond)
                {
                    forBody->reset();
                    m_lexer = forBody;
                    statement(execute);
                }
                if(execute && loopCond)
                {
                    forIter->reset();
                    m_lexer = forIter;
                    CLEAN(base(execute));
                }
            }
            m_lexer = oldLex;
            delete forCond;
            delete forIter;
            delete forBody;
            if(loopCount<=0)
            {
                m_roottable->trace();
                TRACE("FOR Loop exceeded %d iterations at %s\n",
                    TINYJS_LOOP_MAX_ITERATIONS, m_lexer->getPosition().c_str());
                throw new RuntimeError("LOOP_ERROR");
            }
        }
        else if(m_lexer->m_tk == LEX_R_RETURN)
        {
            m_lexer->match(LEX_R_RETURN);
            VarLink* result = 0;
            if(m_lexer->m_tk != ';')
            {
                result = base(execute);
            }
            if(execute)
            {
                VarLink* resultVar = m_scopes.back()->findChild(TINYJS_RETURN_VAR);
                if(resultVar)
                {
                    resultVar->replaceWith(result);
                }
                else
                {
                    TRACE("RETURN statement, but not in a function.\n");
                }
                execute = false;
            }
            CLEAN(result);
            m_lexer->match(';');
        }
        else if(m_lexer->m_tk==LEX_R_FUNCTION)
        {
            VarLink* funcVar = parseFunctionDefinition();
            if(execute)
            {
                if(funcVar->name == TINYJS_TEMP_NAME)
                {
                    TRACE("Functions defined at statement-level are meant to have a name\n");
                }
                else
                {
                    m_scopes.back()->addChildNoDup(funcVar->name, funcVar->var);
                }
            }
            CLEAN(funcVar);
        }
        else
        {
            m_lexer->match(LEX_EOF);
        }
    }

    /// Get the given variable specified by a path (var1.var2.etc), or return 0
    Variable* Interpreter::getScriptVariable(const std::string& path)
    {
        // traverse path
        size_t prevIdx = 0;
        size_t thisIdx = path.find('.');
        if(thisIdx == std::string::npos)
        {
            thisIdx = path.length();
        }
        Variable* var = m_roottable;
        while(var && (prevIdx < path.length()))
        {
            std::string el = path.substr(prevIdx, thisIdx-prevIdx);
            VarLink* varl = var->findChild(el);
            var = varl ? varl->var : 0;
            prevIdx = (thisIdx + 1);
            thisIdx = path.find('.', prevIdx);
            if(thisIdx == std::string::npos)
            {
                thisIdx = path.length();
            }
        }
        return var;
    }

    /// Get the value of the given variable, or return 0
    const std::string* Interpreter::getVariable(const std::string& path)
    {
        Variable* var = getScriptVariable(path);
        // return result
        if(var)
        {
            return &var->getString();
        }
        else
        {
            return 0;
        }
    }

    /// set the value of the given variable, return trur if it exists and gets set
    bool Interpreter::setVariable(const std::string& path, const std::string& varData)
    {
        Variable* var = getScriptVariable(path);
        // return result
        if(var)
        {
            if(var->isInt())
            {
                var->setInt((int)strtol(varData.c_str(),0,0));
            }
            else if(var->isDouble())
            {
                var->setDouble(strtod(varData.c_str(),0));
            }
            else
            {
                var->setString(varData.c_str());
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    /// Finds a child, looking recursively up the m_scopes
    VarLink* Interpreter::findInScopes(const std::string& childName)
    {
        for(int s=m_scopes.size()-1; s>=0; s--)
        {
            VarLink* v = m_scopes[s]->findChild(childName);
            if(v)
            {
                return v;
            }
        }
        return NULL;
    }

    /// Look up in any parent classes of the given object
    VarLink* Interpreter::findInParentClasses(Variable* object, const std::string& name)
    {
        // Look for links to actual parent classes
        VarLink* parentClass = object->findChild(TINYJS_PROTOTYPE_CLASS);
        while(parentClass)
        {
            VarLink* implementation = parentClass->var->findChild(name);
            if(implementation)
            {
                return implementation;
            }
            parentClass = parentClass->var->findChild(TINYJS_PROTOTYPE_CLASS);
        }
        // else fake it for std::strings and finally objects
        if(object->isString())
        {
            VarLink* implementation = m_stringclass->findChild(name);
            if(implementation)
            {
                return implementation;
            }
        }
        if(object->isArray())
        {
            VarLink* implementation = m_arrayclass->findChild(name);
            if(implementation)
            {
                return implementation;
            }
        }
        VarLink* implementation = m_objectclass->findChild(name);
        if(implementation)
        {
            return implementation;
        }
        return 0;
    }


    Variable* Interpreter::getRoot()
    {
        return m_roottable;
    }
}
